# FFT 1D calculation using MPI library

### 1. Source code: 

This program was written on this code: http://monismith.info/cs599/examples/iterFFT.c
The main goal of project was to create a parallel calculations - so the focus was how to **spread the data between processes**, not on FFT algorithm. 


### 2. Main changes: 

Main changes are in splitting data between processes.

* only MASTER reads data from file
* MASTER broadcast the length of array to each SLAVE (which will be used to calculate FFT by each process)

See the diagram below:

![MPI FFT](mpi_fft_schemat.png)

The calculations are the same as in monismith code. All processes (even master) are calculating the Fourier Transform.

### 3. Files: 

the **nr_07-nr_09-Skowronek.pdf** file is a small documentation of this program [PL]