#include <stdio.h>
#include <math.h>
#include <complex.h>
#include <stdlib.h>
#include <mpi.h>

#define PI 3.14159265358979323846

/*

Kompilacja: 	make
Uruchomienie:	make run SAMPLES=../fft_in.txt

*/

unsigned int reverse(unsigned int, int, int);
void fft(double complex *, double complex *, int);

#define DATA_COUNT_TAG 0
int rank, world_size;
int main(int argc, char ** argv)
{
  int inArrSize;
  double start, finish;
  
  /*
  
  */
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  
  double complex * locInArr;
  double complex * locOutArr;
  double complex * resultArr;
  unsigned int dataPortion;
  double complex * inArr;
  
  if(rank == 0)
  {
    /*
      MASTER 
      - opens a file with data,
      - stores the data in array,
      - calculate the amount of data for each processes to pass
      - passes the lenght of their local array  
    */
    double temp;
    FILE * fptr;
    
    // open file with data
    fptr = fopen(argv[1], "r");
    fscanf(fptr, "%d", &inArrSize);
    
    dataPortion = inArrSize/world_size;
    resultArr = (double complex *) malloc(sizeof(double complex)*inArrSize);
    inArr = (double complex *) malloc(sizeof(double complex)*inArrSize);
    locInArr = (double complex *) malloc(sizeof(double complex)*dataPortion);
    locOutArr = (double complex *) malloc(sizeof(double complex)*dataPortion);

    // store data in input array
    for(int i = 0; i < inArrSize; i++) 
    {
      fscanf(fptr, "%lf", &temp);
      inArr[i] = temp;
    }
    fclose(fptr);
    
    /*
      broadcast the lenght (dataPortion) to SLAVES
    */
    MPI_Bcast(&dataPortion, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    
  }
  else
  {
    // receive the length of data portion (SLAVE)
    MPI_Bcast(&dataPortion, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);

    locInArr = (double complex *) malloc(sizeof(double complex)*dataPortion);
    locOutArr = (double complex *) malloc(sizeof(double complex)*dataPortion);
  }

  // send to everyone (including MASTER)
  MPI_Scatter(inArr, dataPortion, MPI_C_DOUBLE_COMPLEX, locInArr, dataPortion, MPI_C_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);

  fft(locInArr, locOutArr, dataPortion);
  
  // gather FFT result from all processes
  MPI_Gather(locOutArr, dataPortion, MPI_C_DOUBLE_COMPLEX, resultArr, dataPortion, 
             MPI_C_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
  
  
  // display result
  if(rank == 0)
  {
    for(int i = 0; i < inArrSize; i++) 
      printf("y[%d] = %lf + %lf*i\n", i, creal(resultArr[i]), cimag(resultArr[i]) );

    free((void *) resultArr);
  }

  free((void *) locInArr);
  free((void *) locOutArr);
  
  //finish=MPI_Wtime(); /*stop timer*/

  MPI_Finalize();
  return 0;
}


void fft(double complex * inArr, double complex * outArr, int arrSize)
{
  int r = (int) ceil(log2(arrSize*world_size));
  int start = 0;
  int end = arrSize;
  double complex * R = malloc(sizeof(double complex)*arrSize);
  double complex * S = malloc(sizeof(double complex)*arrSize);
  double complex * Sk = malloc(sizeof(double complex)*arrSize);
  double complex * temp;

  for(int i = start; i < end; i++)
    R[i] = inArr[i];

  
  for(int m = 0; m < r; m++)
  {
    for(int i = 0; i < arrSize; i++)
    {
      Sk[i] = S[i] = R[i];
    }
    unsigned int bit = 1 << (r - m - 1);
    unsigned int notbit = ~bit;

    int dest, src;
    int splitPoint = world_size / (1 << (m+1));
    
    if(splitPoint > 0)
    {
      /*
        CHOSING THE DATA FOR BUTTERFLY OPERATIONS 
        
        Check which data is needed for FFT multiply an sum 
        1. Set and/or get from other process and multiply by Wk_n factor 
        2. Set and/or get from other process but multiply the one from current process      
      */
      if( ( rank % (splitPoint*2) ) < splitPoint)
      {
        dest = src = rank + splitPoint;
        MPI_Sendrecv(S, arrSize, MPI_C_DOUBLE_COMPLEX, dest, 0,
                     Sk, arrSize, MPI_C_DOUBLE_COMPLEX,  src, 0,
                       MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      }
      else
      {
        dest = src = rank - splitPoint;
        MPI_Sendrecv(Sk, arrSize, MPI_C_DOUBLE_COMPLEX, dest, 0,
                   S, arrSize, MPI_C_DOUBLE_COMPLEX,  src, 0,
                       MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      }
    }
    else
    {
      // when split point is to small: 
      // all operations are inside one process
      for(int i = 0; i < arrSize; i++)
        Sk[i] = S[i];
    }
    for(int i = rank*arrSize, l = 0; l < arrSize; i++, l++)
    {
      /*
    
        Depending on current iteration and size of each portion of data from processes
        choose the correct index of data from arrays (receivend and sent via MPI_Sendrecv
      
      */
      int j = (i & notbit) % arrSize;
      int k = (i | bit) % arrSize;
     
      int expFactor = reverse(i, r, m);
      // multiply & sum:
      R[l] = S[j] + Sk[k] * cexp( (2*PI*I*expFactor)/(arrSize*world_size));
    }
  }

  for(int i = 0; i < arrSize; i++)
  {
    outArr[i] = R[i];
  }

  free((void *) R);
  free((void *) S);
  free((void *) Sk);
}

// reverse to get appropriate exponental factor 
unsigned int reverse(unsigned int x, int r, int m)
{
    unsigned int y = 0;

    x = x >> (r-m-1);

    for(int i = 0; i < m+1; i++)
    {
      y |= x & 1;
      x = x >> 1;
      if(i < m)
        y = y << 1;
    }

    y = y << (r-m-1);

    return y;
}
